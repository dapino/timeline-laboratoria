import React from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import themes from "./themes";
import UserProvider from "./providers/UserProvider";
import Home from "./pages/Home";
import Header from "./components/Layout/Header";
import { pxToRem } from "./utils";

const GlobalStyle = createGlobalStyle`
  body {
    font-family: "Roboto Condensed", sans-serif;
    margin: 0;
    padding: ${pxToRem(70)} 0 ${pxToRem(50)} 0;
    background-color: ${(props) => props.theme.bodyBackground};
  }
`;

const themeContext = {
  theme: themes.light,
  // TODO: implement function to switch to night mode
  // toogleTheme: () => {},
};

function App() {
  return (
    <ThemeProvider theme={themeContext.theme}>
      <UserProvider>
        <Header />
        <Home />
      </UserProvider>
      <GlobalStyle />
    </ThemeProvider>
  );
}

export default App;
