const colors = {
  white: "#fff",
  black: "#000",
  fuchsia: "#F42F63",
  fuchsiaDark: "#7d203e",
  teal: "#459d8e",
  tealDark: "#286357",
  yellow: "#FFE521",
  grayLight: "#CCCCCC",
  gray: "#8d8d8d",
  grayDark: "#4A4A4F",
  lightTeal: "#E2ECED",
  blueGray: "#f2f7fd"
};

const themes = {
  light: {
    colors,
    bodyBackground: colors.blueGray,
    black: colors.black,
    white: colors.white,
    primaryColor: colors.yellow,
    secondaryColor: colors.teal,
    secondaryColorDark: colors.tealDark,
    alertColor: colors.fuchsia,
    cardShadow: `0px 3px 6px -1px ${colors.grayDark}`,
  }
};

export default themes;
