import React, { useState } from "react";
import { auth } from "../services/firebase";
import Label from "../components/Forms/Label";
import Input from "../components/Forms/Input";
import ErrorMessage from "../components/Text/Error";
import Card from "../components/Layout/Card";
import { Col1, Col2, GridContainer, Row } from "../components/Layout/Grid";
import H1 from "../components/Text/H1";
import SubmitButton from "../components/Forms/SubmitButton";

const Login = ({ history }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [user, setUser] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    auth
      .signInWithEmailAndPassword(email, password)
      .then((userData) => {
        setUser(userData);
        history.push("/timeline");
      })
      .catch((error) => {
        setError(`Oops!, ${error.message}`);
      });
  };

  const onChange = (event) => {
    const { name, value } = event.currentTarget;
    if (name === "userEmail") {
      setEmail(value);
    } else if (name === "userPassword") {
      setPassword(value);
    }
  };

  return (
    <GridContainer>
      <Row center>
        <Col2>
          <Card column center>
            <H1>Sign In</H1>
            {error !== null && <ErrorMessage>{error}</ErrorMessage>}
            <form onSubmit={handleSubmit}>
              <Row>
                <Col1>
                  <Label htmlFor="userEmail">Email:</Label>
                  <Input
                    type="email"
                    name="userEmail"
                    defaultValue={email}
                    placeholder="youremail@laboratoria.la"
                    id="userEmail"
                    onChange={(event) => onChange(event)}
                    required
                  />
                </Col1>
                <Col1>
                  <Label htmlFor="userPassword">Password:</Label>
                  <Input
                    type="password"
                    name="userPassword"
                    defaultValue={password}
                    placeholder="Your password"
                    id="userPassword"
                    onChange={(event) => onChange(event)}
                    required
                  />
                </Col1>
                <Col1 center>
                  <SubmitButton value="Come in!" />
                </Col1>
                <Col1>
                  {user !== null && <span>{`Welcome ${user.user.email}`}</span>}
                </Col1>
              </Row>
            </form>
          </Card>
        </Col2>
      </Row>
    </GridContainer>
  );
};

// TODO: prop-types validation

export default Login;
