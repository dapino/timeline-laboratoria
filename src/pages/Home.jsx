import React, { useContext } from "react";
// import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Timeline from "./Timeline";
import Login from "./Login";
import { UserContext } from "../providers/UserProvider";

const Home = () => {
  const user = useContext(UserContext);
  // const PrivateRoute = ({ component: Component, ...rest }) => {
  //   return (
  //     <Route
  //       {...rest}
  //       render={(routeProps) =>
  //         user === null ? <Component {...routeProps} /> : <Redirect to={"/login"} />
  //       }
  //     />
  //   );
  // };

  // TODO: configure router with private routes
  return (
    // <BrowserRouter>
    //   <Switch>
    user ? <Timeline /> : <Login />
    //   </Switch>
    // </BrowserRouter>
  );
};

export default Home;
