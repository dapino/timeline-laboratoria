import React, { useState } from "react";
import { createPostInFirebase } from "../services/firebase";
import { Col1, GridContainer, Row } from "../components/Layout/Grid";
import Footer from "../components/Layout/Footer";
import SinglePost from "../components/Post/SinglePost";
import P from "../components/Text/P";
import usePosts from "../hooks/usePosts";
import Card from "../components/Layout/Card";
import H2 from "../components/Text/H2";
import Label from "../components/Forms/Label";
import Input from "../components/Forms/Input";
import Textarea from "../components/Forms/TextArea";
import SubmitButton from "../components/Forms/SubmitButton";
import Notification from "../components/Layout/Notification";

const Timeline = () => {
  const { postsList, setLoading, loading } = usePosts();
  const [created, setCreated] = useState(false);
  const [postTitle, setPostTitle] = useState("");
  const [postContent, setPostContent] = useState("");
  // TODO: create input file
  // const [postImage, setPostImage] = useState("");
  const clearState = () => {
    setPostTitle("");
    setPostContent("");
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    clearState();
    // TODO: change name of the key "text" by "content"
    const newPostData = {
      id: Date.now().toString(),
      title: postTitle,
      content: postContent,
    };
    // TODO: improve response management
    createPostInFirebase(newPostData).then(() => {
      setLoading(true);
      setCreated(true);
      // TODO: create modal or toast notification when the post is created
      setTimeout(() => {
        setCreated(false);
      }, 3000);
      setLoading(false);
    });
  };

  const onChange = (event) => {
    const { name, value } = event.currentTarget;
    if (name === "postTitle") {
      setPostTitle(value);
    } else if (name === "postContent") {
      setPostContent(value);
    }
  };

  const renderPosts = () => (
    <Row center column>
      {postsList.map((post) => {
        return (
          <SinglePost key={post.id} title={post.title} text={post.content} />
        );
      })}
    </Row>
  );

  return (
    <GridContainer>
      <Card column width={400}>
        <H2>Dear diary</H2>
        {
          // TODO: Implement error message
          /*<ErrorMessage>{error !== null && <div>{error}</div>}</ErrorMessage>*/
        }
        <form onSubmit={handleSubmit}>
          <Row>
            <Col1>
              <Label htmlFor="postText">What's the title of the new post</Label>
              <Input
                name="postTitle"
                value={postTitle}
                placeholder="Write the title"
                id="postTitle"
                onChange={(event) => onChange(event)}
                required
              />
            </Col1>
            <Col1>
              <Label htmlFor="postText">Write the content</Label>
              <Textarea
                name="postContent"
                value={postContent}
                placeholder="Not too long :)"
                id="postContent"
                onChange={(event) => onChange(event)}
                required
              />
            </Col1>
            <Col1 center>
              <SubmitButton value="Create new post" />
            </Col1>
          </Row>
        </form>
      </Card>
      {loading ? (
        <Card>
          <Col1 center>
            <P>Fetching posts...</P>
          </Col1>
        </Card>
      ) : (
        renderPosts()
      )}
      {created && (
        <Notification>
          <P>your post has been created!</P>
        </Notification>
      )}
      <Footer />
    </GridContainer>
  );
};

export default Timeline;
