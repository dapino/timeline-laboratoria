import styled from "styled-components";
import { pxToRem } from "../../utils";

const P = styled.p`
  font-size: ${pxToRem(16)};
  font-family: "Roboto Condensed", sans-serif;
  font-weight: 300;
  margin-top: ${pxToRem(8)};
`;

export default P;
