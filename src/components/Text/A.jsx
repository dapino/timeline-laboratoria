import styled from "styled-components";
import { pxToRem } from "../../utils";

const A = styled.a`
  font-size: ${pxToRem(14)};
  color: ${(props) => props.theme.secondaryColor};
  font-weight: 300;
`;

export default A;
