import styled from "styled-components";
import { pxToRem } from "../../utils";

const H2 = styled.h2`
  font-size: ${pxToRem(18)};
  color: ${(props) => props.theme.secondaryColorDark};
  font-family: "Roboto Slab", serif;
`;

export default H2;
