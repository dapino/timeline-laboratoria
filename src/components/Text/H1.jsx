import styled from "styled-components";
import { pxToRem } from "../../utils";

const H1 = styled.h1`
  font-size: ${pxToRem(24)};
  color: ${(props) => props.theme.secondaryColorDark};
  font-family: "Roboto Slab", serif;
`;

export default H1;
