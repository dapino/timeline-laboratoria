import styled from "styled-components";
import { pxToRem } from "../../utils";

const ErrorMessage = styled.span`
  font-size: ${pxToRem(14)};
  color: ${(props) => props.theme.alertColor};
  border: 1px dashed ${(props) => props.theme.alertColor};
  padding: ${pxToRem(4)};
  font-weight: 300;
`;

export default ErrorMessage;
