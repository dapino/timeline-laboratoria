import styled from "styled-components";
import { pxToRem } from "../../utils";

const H3 = styled.h3`
  font-size: ${pxToRem(16)};
  color: ${(props) => props.theme.secondaryColor};
  font-family: "Roboto Condensed", sans-serif;
  font-weight: 400;
  text-transform: uppercase;
  text-decoration: underline;
  margin: 0;
`;

export default H3;
