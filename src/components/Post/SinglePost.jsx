import React from "react";
import PropTypes from "prop-types";
import Card from "../Layout/Card";
import H3 from "../Text/H3";
import P from "../Text/P";
import { Col1, Row } from "../Layout/Grid";

const SinglePost = (props) => {
  // TODO: fix this ESlint warning: title and text is missing in props validation
  const { title, text } = props;
  return (
    <Card width={400}>
      <Row>
        <Col1 column>
          <H3>{title}</H3>
          <P>{text}</P>
        </Col1>
      </Row>
    </Card>
  );
};

SinglePost.defaultProps = {
  title: PropTypes.string,
  text: PropTypes.string,
};

export default SinglePost;
