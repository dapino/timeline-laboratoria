import styled from "styled-components";
import { pxToRem } from "../../utils";

const Notification = styled.div`
  display: flex;
  flex-direction: ${(props) => (props.column ? "column" : "row")};
  align-items: ${(props) => (props.center ? "column" : "row")};
  padding: ${pxToRem(8)};
  background-color: ${(props) => props.theme.primaryColor};
  box-shadow: ${(props) => props.theme.cardShadow};
  position: fixed;
  bottom: ${pxToRem(50)};
  right: ${pxToRem(15)};
  box-sizing: border-box;
  width: ${(props) => (props.width ? `${props.width}px` : "auto")};
  z-index: 1;
  p {
    font-size: ${pxToRem(12)};
    margin: 0;
  }
`;

export default Notification;
