import React, { useContext } from "react";
import styled from "styled-components";
import { auth } from "../../services/firebase";
import { UserContext } from "../../providers/UserProvider";
import { ButtonSecondary } from "../Forms/Button";
import A from "../Text/A";
import { pxToRem } from "../../utils";

export const StyledFooter = styled.footer`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${pxToRem(15)};
  position: fixed;
  bottom: 0;
  height: ${pxToRem(40)};
  left: 0;
  right: 0;
  background-color: ${(props) => props.theme.colors.black};
  box-sizing: border-box;
`;

const Footer = ({history}) => {
  const user = useContext(UserContext);
  return (
    <StyledFooter>
      <A
        href="https://laboratoria.la"
        target="_blank"
        rel="noopener noreferrer"
      >
        Laboratoria.la
      </A>
      {user ? (
        <ButtonSecondary
          onClick={() => {
            auth.signOut();
          }}
        >
          Logout
        </ButtonSecondary>
      ) : (
        <p>:)</p>
      )}
    </StyledFooter>
  );
};

export default Footer;
