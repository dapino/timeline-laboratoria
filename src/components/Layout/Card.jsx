import styled from "styled-components";
import { pxToRem } from "../../utils";

const Card = styled.div`
  display: flex;
  flex-direction: ${props => props.column ? 'column' : 'row'};
  align-items: ${props => props.center ? 'column' : 'row'};
  padding: ${pxToRem(16)};
  background-color: ${(props) => props.theme.colors.white};
  box-shadow: ${(props) => props.theme.cardShadow};
  margin: ${pxToRem(8)} auto;
  position: relative;
  box-sizing: border-box;
  width: ${props => props.width ? `${props.width}px` : 'auto'};
`;

export default Card;
