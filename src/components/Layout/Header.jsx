import React from "react";
import styled from "styled-components";
import { pxToRem } from "../../utils";

export const NavBar = styled.header`
  display: flex;
  align-items: center;
  height: ${pxToRem(50)};
  background-color: ${(props) => props.theme.black};
  padding: ${pxToRem(15)};
  box-sizing: border-box;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
`;

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  height: 100%;
`;

export const Logo = styled.span`
  color: ${(props) => props.theme.primaryColor};
  font-family: "Roboto Slab", sans-serif;
  font-size: ${pxToRem(12)};
  text-decoration: line-through;
`;

const Header = () => {
  return (
    <NavBar>
      <Nav>
        <Logo>Not yet another social network</Logo>
      </Nav>
    </NavBar>
  );
};

export default Header;
