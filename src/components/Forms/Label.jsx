import styled from "styled-components";
import { pxToRem } from "../../utils";

const Label = styled.label`
  font-size: ${pxToRem(12)};
  margin-top: ${pxToRem(16)};
  font-weight: 300;
  color: ${(props) => props.theme.secondaryColorDark};
`;

export default Label;
