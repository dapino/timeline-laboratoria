import styled from "styled-components";
import { pxToRem } from "../../utils";

const Textarea = styled.textarea`
  box-sizing: border-box;
  padding: ${pxToRem(8)} ${pxToRem(16)};
  margin-top: ${pxToRem(5)};
  width: 100%;
  outline: none;
  border: 1px solid ${(props) => props.theme.primaryColor};
`;

export default Textarea;
