import React from "react";
import styled from "styled-components";
import { pxToRem } from "../../utils";

const StyledSubmit = styled.input`
  display: inline-block;
  color: ${(props) => props.theme.colors.white};
  background-color: ${(props) => props.theme.secondaryColor};
  border: solid ${pxToRem(1)} transparent;
  font-size: ${pxToRem(14)};
  padding: ${pxToRem(6)} ${pxToRem(24)};
  margin: ${pxToRem(16)} ${pxToRem(16)} ${pxToRem(8)};
  text-decoration: none;
  text-transform: lowercase;
  transition: all 0.2s ease;
  cursor: pointer;
  &::first-letter {
    text-transform: uppercase;
  }
  &:hover {
    border: solid ${pxToRem(1)} ${(props) => props.theme.white};
    color: ${(props) => props.theme.white};
    background-color: ${(props) => props.theme.secondaryColorDark};
  }
`;

const SubmitButton = ({value}) => {
  return <StyledSubmit type="submit" value={value}/>;
};

export default SubmitButton;
