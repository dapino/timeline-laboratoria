import styled from "styled-components";
import { pxToRem } from "../../utils";

export const Button = styled.button`
  display: inline-block;
  color: ${(props) => props.theme.colors.white};
  background-color: ${(props) => props.theme.secondaryColor};
  border: solid ${pxToRem(1)} transparent;
  font-size: ${pxToRem(14)};
  padding: ${pxToRem(6)} ${pxToRem(24)};
  margin: ${pxToRem(16)} ${pxToRem(16)} ${pxToRem(8)};
  text-decoration: none;
  text-transform: lowercase;
  transition: all 0.2s ease;
  cursor: pointer;
  &::first-letter {
    text-transform: uppercase;
  }
  &:hover {
    border: solid ${pxToRem(1)} ${(props) => props.theme.white};
    color: ${(props) => props.theme.white};
    background-color: ${(props) => props.theme.secondaryColorDark};
  }
`;

export const ButtonSecondary = styled(Button)`
  color: ${(props) => props.theme.primaryColor};
  background-color: transparent;
  border: solid ${pxToRem(1)} ${(props) => props.theme.primaryColor};
  padding: ${pxToRem(3)} ${pxToRem(24)};
  font-size: ${pxToRem(12)};
  margin: 0;
  &:hover {
    border: solid ${pxToRem(1)} ${(props) => props.theme.primaryColor};
    color: ${(props) => props.theme.primaryColor};
    background-color: transparent;
  }
`;
