/*
 pxToRem: convert px units to rem
 @target: {number}
 */
const BASE_FONT_SIZE = 16;
export const pxToRem = (target) => `${(target / BASE_FONT_SIZE)}rem`;

/*
 removeEmpty: remove empty elements in an object
 @obj: {object}
 */
export const removeEmpty = obj =>
  Object.fromEntries(
    Object.entries(obj).filter(([k, v]) => ((v !== null && v !== "") || (v === false)))
  );
