import firebase from "firebase";

const config = {
  apiKey: "AIzaSyCe81kJ0ajaVGDou9CLcaf7_LovscBV7EY",
  authDomain: "another-social.firebaseapp.com",
  databaseURL: "https://another-social.firebaseio.com",
  projectId: "another-social",
  storageBucket: "another-social.appspot.com",
  messagingSenderId: "406233042585",
  appId: "1:406233042585:web:5bf9a56814b04b340d7208"

};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const getPostsFromFirebase = async () => {
  const posts = [];
  await firestore
    .collection("posts")
    .orderBy("id", "desc")
    .get()
    .then((querySnapshot) => {
      querySnapshot.docs.forEach((doc) => {
        posts.push(doc.data());
      });
    });
  return posts;
};

export const createPostInFirebase = async (newPostData) => {
  await firestore
    .collection("posts")
    .doc(newPostData.id)
    .set(newPostData)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.error("Error writing document: ", error);
    });
};
