import React from "react";
import { render } from "@testing-library/react";
import Header from "../../components/Layout/Header";

// TODO: implement testing
describe("<Header />", () => {
  const header = render(<Header />);
  test("Render Header component", () => {
    expect(header.length).toEqual(1);
  });
  test("Render Header logo", () => {
    expect(header.find("span").text()).toEqual(
      "Not yet another social network"
    );
  });
});
