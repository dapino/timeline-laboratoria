import { useEffect, useState } from "react";
import { getPostsFromFirebase } from "../services/firebase";

const usePosts = () => {
  const [loading, setLoading] = useState(false);
  const [postsList, setPostsList] = useState([]);

  useEffect(() => {
    setLoading(true);
    getPostsFromFirebase().then((posts) => setPostsList(posts));
    setLoading(false);
  }, [loading]);
  return { postsList, setPostsList, loading, setLoading };
};

export default usePosts;
